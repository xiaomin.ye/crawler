// <script src="https://code.jquery.com/jquery-3.6.1.min.js"
//         integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossOrigin="anonymous"></script>

const fs = require('fs')
const cheerio = require('cheerio');
const html = fs.readFileSync('./index.html', {encoding: 'utf-8'})
const $ = cheerio.load(html);
let Product_Image_URL = $('#imgTagWrapperId img').attr('src')
let Seller_URL = $('#sellerProfileTriggerId').attr('href')
let Product_Price = $("#corePrice_desktop > div > table > tbody > tr:nth-child(2) > td.a-span12 > span.a-price.a-text-price.a-size-medium.apexPriceToPay > span.a-offscreen").text() || $('#sns-base-price').text().trim().split('\n')[0].trim() || $("#corePriceDisplay_desktop_feature_div > div.a-section.a-spacing-none.aok-align-center > span > span.a-offscreen").text()
const Seller = $('#sellerProfileTriggerId').text()
console.log(Product_Image_URL, Seller_URL)
console.log(Product_Price)

